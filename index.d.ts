import { Client, Namespace, ApiClient } from '../collab.io-networking';

export class TestEnvironment {

    public PeerCount: number;
    public Namespace: Namespace;
    public Client: Client;
    public constructor(client: Client);
    public Dispose();
    public WaitForOthers(number: number): Promise<void>;
    public Delay(ms: number): Promise<void>;
}

export abstract class TestScenario {

    public Results;
    public Begin(peers: number, client: Client, apiClient: ApiClient): Promise<void>;
    public Dispose();
}

export class CorrectnessTest extends TestScenario {

}

export class LatencyTest extends TestScenario {

}

export class FunctionalityTest extends TestScenario {

}

export class BandwidthTest extends TestScenario {

}

export class StressTest extends TestScenario {

}