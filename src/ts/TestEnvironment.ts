import { Client, Namespace, ApiClient } from '../../../collab.io-networking';

export class TestEnvironment {

    protected ns: Namespace;
    protected peers: number[];

    public get PeerCount() {
        return this.peerCount;
    }

    public get Namespace(): Namespace {
        return this.ns;
    }

    public get Client(): Client {
        return this.client;
    }

    public get Api(): ApiClient {
        return this.apiClient;
    }

    public constructor(protected peerCount: number, 
        protected client: Client,
        protected apiClient: ApiClient) {
        this.peers = [];

        this.ns = client.Room('general').Namespace('chat');
    }

    public Dispose() {
        this.client.Disconnect();
    }

    public WaitForOthers(number: number) {
        return new Promise<void>(resolve => {
            this.ns.On('newPeer', data => {
                this.peers.push(data);

                if (this.peers.length == number)
                    resolve();
            });
        });
    }

    public Delay(ms: number) {
        return new Promise<void>(resolve => {
            setTimeout(resolve, ms);
        });
    }
}