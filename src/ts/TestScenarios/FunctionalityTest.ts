import { TestScenario } from '../TestScenario';
import { TestEnvironment } from '../TestEnvironment';
import { EditorController } from '../../../../collab.io-client-api';

export class FunctionalityTest extends TestScenario {

    protected async Run(environment: TestEnvironment) {
        let peers = environment.PeerCount;

        environment.Api.AuthToken = 'abc';
        let editor = new EditorController(environment.Client, environment.Api, 1);
        let responded = 0;
        return new Promise<void>(async resolve => {

            await environment.WaitForOthers(peers);

            console.log(await editor.LoadProject());

            resolve();
        });
    }
}