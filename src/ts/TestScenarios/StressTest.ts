import {TestScenario} from '../TestScenario';
import {TestEnvironment} from '../TestEnvironment';

export class StressTest extends TestScenario {

    protected async Run(environment: TestEnvironment) {
        let peers = environment.PeerCount;

        environment.Client.On('ping', (data, sender) => {
            environment.Client.EmitTo(sender, 'pong', Date.now());
        });

        await environment.WaitForOthers(peers);

        let latencies = {};
        let responded = 0;
        return new Promise<void>(resolve => {

            environment.Client.On('pong', (data, sender) => {
                let finish = Date.now();

                latencies[sender.Id].pong = data;
                latencies[sender.Id].latency = {
                    oneWay: (finish - latencies[sender.Id].ping) * 0.5,
                    roundTrip: finish - latencies[sender.Id].ping
                };

                responded++;
                if (responded == peers) {
                    let oneWay = 0;
                    let roundTrip = 0;
                    for (let p in latencies) {
                        oneWay += latencies[p].latency.oneWay;
                        roundTrip += latencies[p].latency.roundTrip;
                    }

                    this.results.latency = {
                        oneWay: oneWay / peers,
                        roundTrip: roundTrip / peers
                    };
                    resolve();
                }
            });

            for (let peer of environment.Namespace.Peers) {
                let ping = Date.now();
                environment.Client.EmitTo(peer, 'ping', ping);

                latencies[peer.Id] = { ping };
            }
        });
    }
}