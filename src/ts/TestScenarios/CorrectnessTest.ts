import { TestScenario } from '../TestScenario';
import { TestEnvironment } from '../TestEnvironment';

export class CorrectnessTest extends TestScenario {

    protected async Run(environment: TestEnvironment) {
        let peers = environment.PeerCount;
        let responded = 0;

        return new Promise<void>(async resolve => {

            environment.Client.On('ping', (data, sender) => {
                environment.Client.EmitTo(sender, 'pong', Date.now());
            });

            environment.Client.On('pong', (data, sender) => {
                responded++;

                if (responded == peers) {
                    this.results.ok = true;
                    resolve();
                }
            });

            await environment.WaitForOthers(peers);

            for (let peer of environment.Namespace.Peers) {
                environment.Client.EmitTo(peer, 'ping', Date.now());
            }
        });
    }
}