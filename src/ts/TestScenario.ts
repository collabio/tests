import { TestEnvironment } from './TestEnvironment';
import { Client, ApiClient } from '../../../collab.io-networking';

export abstract class TestScenario {

    protected environment: TestEnvironment;
    protected results: any;

    public get Results() {
        return this.results;
    }

    public async Begin(peers: number, client: Client, apiClient: ApiClient) {
        this.environment = new TestEnvironment(peers, client, apiClient);
        this.results = {};

        await this.Run(this.environment);
    }

    public Dispose() {
        this.environment.Dispose();
    }

    protected abstract async Run(environment: TestEnvironment);
}