"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const TestScenario_1 = require("../TestScenario");
class CorrectnessTest extends TestScenario_1.TestScenario {
    Run(environment) {
        return __awaiter(this, void 0, void 0, function* () {
            let peers = environment.PeerCount;
            let responded = 0;
            return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
                environment.Client.On('ping', (data, sender) => {
                    environment.Client.EmitTo(sender, 'pong', Date.now());
                });
                environment.Client.On('pong', (data, sender) => {
                    responded++;
                    if (responded == peers) {
                        this.results.ok = true;
                        resolve();
                    }
                });
                yield environment.WaitForOthers(peers);
                for (let peer of environment.Namespace.Peers) {
                    environment.Client.EmitTo(peer, 'ping', Date.now());
                }
            }));
        });
    }
}
exports.CorrectnessTest = CorrectnessTest;
