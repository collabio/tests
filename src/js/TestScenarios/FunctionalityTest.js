"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const TestScenario_1 = require("../TestScenario");
const collab_io_client_api_1 = require("../../../../collab.io-client-api");
class FunctionalityTest extends TestScenario_1.TestScenario {
    Run(environment) {
        return __awaiter(this, void 0, void 0, function* () {
            let peers = environment.PeerCount;
            environment.Api.AuthToken = 'abc';
            let editor = new collab_io_client_api_1.EditorController(environment.Client, environment.Api, 1);
            let responded = 0;
            return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
                yield environment.WaitForOthers(peers);
                console.log(yield editor.LoadProject());
                resolve();
            }));
        });
    }
}
exports.FunctionalityTest = FunctionalityTest;
