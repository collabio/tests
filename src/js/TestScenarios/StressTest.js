"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const TestScenario_1 = require("../TestScenario");
class StressTest extends TestScenario_1.TestScenario {
    Run(environment) {
        return __awaiter(this, void 0, void 0, function* () {
            let peers = environment.PeerCount;
            environment.Client.On('ping', (data, sender) => {
                environment.Client.EmitTo(sender, 'pong', Date.now());
            });
            yield environment.WaitForOthers(peers);
            let latencies = {};
            let responded = 0;
            return new Promise(resolve => {
                environment.Client.On('pong', (data, sender) => {
                    let finish = Date.now();
                    latencies[sender.Id].pong = data;
                    latencies[sender.Id].latency = {
                        oneWay: (finish - latencies[sender.Id].ping) * 0.5,
                        roundTrip: finish - latencies[sender.Id].ping
                    };
                    responded++;
                    if (responded == peers) {
                        let oneWay = 0;
                        let roundTrip = 0;
                        for (let p in latencies) {
                            oneWay += latencies[p].latency.oneWay;
                            roundTrip += latencies[p].latency.roundTrip;
                        }
                        this.results.latency = {
                            oneWay: oneWay / peers,
                            roundTrip: roundTrip / peers
                        };
                        resolve();
                    }
                });
                for (let peer of environment.Namespace.Peers) {
                    let ping = Date.now();
                    environment.Client.EmitTo(peer, 'ping', ping);
                    latencies[peer.Id] = { ping };
                }
            });
        });
    }
}
exports.StressTest = StressTest;
