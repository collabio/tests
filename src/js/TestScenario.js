"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const TestEnvironment_1 = require("./TestEnvironment");
class TestScenario {
    get Results() {
        return this.results;
    }
    Begin(peers, client, apiClient) {
        return __awaiter(this, void 0, void 0, function* () {
            this.environment = new TestEnvironment_1.TestEnvironment(peers, client, apiClient);
            this.results = {};
            yield this.Run(this.environment);
        });
    }
    Dispose() {
        this.environment.Dispose();
    }
}
exports.TestScenario = TestScenario;
