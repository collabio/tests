"use strict";
class TestEnvironment {
    constructor(peerCount, client, apiClient) {
        this.peerCount = peerCount;
        this.client = client;
        this.apiClient = apiClient;
        this.peers = [];
        this.ns = client.Room('general').Namespace('chat');
    }
    get PeerCount() {
        return this.peerCount;
    }
    get Namespace() {
        return this.ns;
    }
    get Client() {
        return this.client;
    }
    get Api() {
        return this.apiClient;
    }
    Dispose() {
        this.client.Disconnect();
    }
    WaitForOthers(number) {
        return new Promise(resolve => {
            this.ns.On('newPeer', data => {
                this.peers.push(data);
                if (this.peers.length == number)
                    resolve();
            });
        });
    }
    Delay(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        });
    }
}
exports.TestEnvironment = TestEnvironment;
